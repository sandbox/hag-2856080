
README
--------------------------------------------------------------------------
This module allows Administrators to Monitor the status of files within 
the system. In a real world senario there is more than one administrator
in the site. The communication maybe broken and the only way to catch 
a file been deleted is inly through the logs. This module provides another
layer to sufficient monitor the status of the files.

Notice:
- Please check if cron is running correctly if Monfis does not update the files.

REQUIREMENTS
------------
This module requires the following modules:
 * File (https://www.drupal.org/docs/7/core/modules/file/)
 * ctools (https://www.drupal.org/project/ctools)
 * File Entity (fieldable files) (https://www.drupal.org/project/file_entity)

INSTALLATION
--------------------------------------------------------------------------
1. Copy the monfis.module to your modules directory.
2. Enable module, database schemas will be setup automatically.
3. Grant users the permission "Administer monfis" so they can use monfis.
4. Run Cron.

The Monfis will run with Drupal's cron.php, and will update the monfis table.
